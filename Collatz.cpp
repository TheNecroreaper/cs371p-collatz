// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream

#include "Collatz.hpp"

using namespace std;

int cache[1000000] = {0};
// ------------
// collatz_read
// ------------

istream& collatz_read (istream& r, int& i, int& j) {
    return r >> i >> j;
}

// ------------
// collatz_eval
// ------------

int collatz_eval (int i, int j) {
    if (!(i > 0 && i < 1000000 && j > 0 && j < 1000000))
        return 0;

    /* set the start to the smallest number */
    int start = i;
    int end = j;
    if (i > j) {
        start = j;
        end = i;
    }

    int max = 0;
    for (int cur = start; cur <= end; cur++) {
        int cycleLength = collatz((unsigned long long) cur);
        if (cycleLength > max)
            max = cycleLength;
    }
    return max;
}

//collatz recursive method
int collatz (unsigned long long i) {
    /* return defined cycle lengths */
    if (i <= 1)
        return 1;
    if (i < 1000000 && cache[i] != 0)
        return cache[i];

    int cycleLength;
    if (i % 2 == 0)
        cycleLength = 1 + collatz(i / 2);
    else
        cycleLength = 2 + collatz((3 * i + 1)/2);

    /* if i is covered by the cache, store the cycle length */
    if (i < 1000000)
        cache[i] = cycleLength;

    return cycleLength;
}
// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    int i;
    int j;
    while (collatz_read(r, i, j)) {
        const int v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
