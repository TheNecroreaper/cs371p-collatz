// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl, istream
#include <sstream>  // istringtstream, ostringstream

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// -----------
// TestCollatz
// -----------

// ----
// read
// ----

TEST(CollatzFixture, read) {
    istringstream r("1 10\n");
    int i;
    int j;
    istream& s = collatz_read(r, i, j);
    ASSERT_TRUE(s);
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 10);
}

// ----
// eval
// ----

TEST(CollatzFixture, eval_1) {
    const int v = collatz_eval(1, 10);
    ASSERT_EQ(v, 20);
}

TEST(CollatzFixture, eval_2) {
    const int v = collatz_eval(100, 200);
    ASSERT_EQ(v, 125);
}

TEST(CollatzFixture, eval_3) {
    const int v = collatz_eval(201, 210);
    ASSERT_EQ(v, 89);
}

TEST(CollatzFixture, eval_4) {
    const int v = collatz_eval(900, 1000);
    ASSERT_EQ(v, 174);
}

TEST(CollatzFixture, eval_5) {
    const int v = collatz_eval(0, 10);
    ASSERT_EQ(v, 0);
}

TEST(CollatzFixture, eval_6) {
    const int v = collatz_eval(1, 1000001);
    ASSERT_EQ(v, 0);
}

TEST(CollatzFixture, eval_7) {
    const int v = collatz_eval(1, 1);
    ASSERT_EQ(v, 1);
}

TEST(CollatzFixture, eval_8) {
    const int v = collatz_eval(2, 1);
    ASSERT_EQ(v, 2);
}
// ----
// collatz
// ----

TEST(CollatzFixture, collatz_1) {
    const int v = collatz(1);
    ASSERT_EQ(v, 1);
}

TEST(CollatzFixture, collatz_2) {
    const int v = collatz(2);
    ASSERT_EQ(v, 2);
}

TEST(CollatzFixture, collatz_3) {
    const int v = collatz(3);
    ASSERT_EQ(v, 8);
}

TEST(CollatzFixture, collatz_4) {
    const int v = collatz(0);
    ASSERT_EQ(v, 1);
}
// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream w;
    collatz_print(w, 1, 10, 20);
    ASSERT_EQ(w.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream r("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("1 10 20\n100 200 125\n201 210 89\n900 1000 174\n", w.str());
}
